const express = require('express')
const router = express.Router()
const getListOfDatabaseController = require('../controllers/getListOfDatabase')
const getTableController = require('../controllers/getTableList')
const getTableColumnController = require('../controllers/getTableColumn')
const getTableDataController = require('../controllers/getTableData')
const exportDataController = require('../controllers/exportData')
const editDataController = require('../controllers/editData')
let config = require('../config/config')

let getUrlPrefix = config.app.prefix
console.log('getUrlPrefix: ', getUrlPrefix)

/* GET home page. */
router.get('/', function (req, res) {
  res.render('index', { title: 'Express' })
})

router.post(getUrlPrefix + '/getTable', function (req, res) {
  getTableController.getTable(req, res)
})

router.post(getUrlPrefix + '/getListOfDatabase', function (req, res) {
  getListOfDatabaseController.getListOfDatabase(req, res)
})

router.post(getUrlPrefix + '/getTableColumn', function (req, res) {
  getTableColumnController.getTableColumn(req, res)
})

router.post(getUrlPrefix + '/getTableData', function (req, res) {
  getTableDataController.getTableData(req, res)
})

router.post(getUrlPrefix + '/exportData', function (req, res) {
  exportDataController.exportData(req, res)
})

router.post(getUrlPrefix + '/getAllDataFromDb', function (req, res) {
  getTableDataController.getAllDataFromDb(req, res)
})

router.post(getUrlPrefix + '/editData', function (req, res) {
  editDataController.editData(req, res)
})
module.exports = router

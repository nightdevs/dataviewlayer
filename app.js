let express = require('express')
let path = require('path')
let cookieParser = require('cookie-parser')
let logger = require('morgan')
let indexRouter = require('./routes/index')
let app = express()
let cors = require('cors')

global._ = require('underscore')
global.q = require('q')
global.request = require('request')
global.MongoClient = require('mongodb').MongoClient
global.mongoPool = require('./utils/mongoDb').mongoPool
global.mysql = require('mysql')
global.pool = require('./utils/db')
global.cryptoJs = require('crypto-js')
global.secretkey = '123456$#@$^@1ERF'
global.config = require('./config/config')
global.getDecryptKey = require('./controllers/commonFunctions').getDecryptKey
global.getEncryptKey = require('./controllers/commonFunctions').getEncryptKey

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({
  extended: false
}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())
app.use('/', indexRouter)

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
})
const expressSwagger = require('express-swagger-generator')(app)
// swagger definition

let options = {
  swaggerDefinition: {
    info: {
      description: 'This is a sample server',
      title: 'Swagger',
      version: '1.0.0'
    },
    produces: [
      'application/json',
      'application/xml'
    ],
    schemes: ['http', 'https'],
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description: ''
      }
    }
  },
  basedir: __dirname, // app absolute path
  files: ['./swaggerDocs/swagger.js'] // Path to the API handle folder
}

expressSwagger(options)

let port = process.env.PORT || config.server.port
let server = app.listen(port)
console.log('Api is running on port', port)
module.exports = app

let mysqlConnect = require('./commonFunctions').mysqlConnect
let connectDB = require('./commonFunctions').connectDB
let formmatRes = require('./commonFunctions').formmatRes

async function mysqlGetTable (reqBody) {
  let deferred = q.defer()
  var poolCon = ''
  try {
    poolCon = await mysqlConnect(reqBody.dbDetails)
  } catch (err) {
    return deferred.reject(err)
  }
  try {
    poolCon.getConnection(function (err, connection) {
      if (err) {
        return deferred.reject(err)
      } else {
        let query = "SELECT TABLE_NAME  FROM INFORMATION_SCHEMA.TABLES  where Table_TYPE like 'BASE TABLE'  and TABLE_SCHEMA='" + reqBody.dbDetails.sql_db + "'"
        connection.query(query, (err, rows) => {
          connection.destroy()
          if (err) {
            return deferred.reject(err)
          } else {
            if (!_.isEmpty(rows)) {
              deferred.resolve(rows)
            } else {
              deferred.resolve({ message: 'No Table Found' })
            }
          }
        })
      }
    })
  } catch (err) {
    return deferred.reject({ Status: 'Failure -: ' + err })
  }

  return deferred.promise
}

function getAllTables (dbInfo, dataSource) {
  let deferred = q.defer()
  let dbDetails = {
    conDetails: dbInfo,
    commandType: 'getCollections'
  }
  connectDB(dbDetails, dataSource)
    .then(dbRes => {
      if (_.isEmpty(dbRes)) {
        deferred.resolve({
          message: 'No Data Found'
        })
      } else {
        let formatDetails = {
          data: dbRes,
          keyName: 'table'
        }
        return formmatRes(formatDetails)
      }
    })
    .then(formatRes => {
      deferred.resolve(formatRes)
    })
    .catch(err => {
      return deferred.reject(err)
    })

  return deferred.promise
}

function getTable (req, res) {
  let reqBody = req.body
  let dataSource = reqBody.dataSource

  if (!_.isEmpty(dataSource) && dataSource.toLowerCase() === 'mysql') {
    mysqlGetTable(reqBody)
      .then((result) => {
        res.status(200).send(result)
      })
      .catch((err) => {
        res.status(400).send(err)
      })
  } else if (!_.isEmpty(dataSource) && dataSource.toLowerCase() === 'mongodb') {
    // let encodePassword = encodeURIComponent(password)
    // reqBody.dbInfo.password = encodePassword
    getAllTables(reqBody, 'getData')
      .then((result) => {
        res.status(200).send(result)
      })
      .catch((err) => {
        res.status(400).send(err)
      })
  } else {
    res.status(200).send({
      message: !_.isEmpty(dataSource) ? 'This DataSource is not exists' : 'DataSource Is Required'
    })
  }
}

module.exports.getTable = getTable

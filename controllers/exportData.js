let getCon = require('./commonFunctions').getCon

function exportData (req, res) {
  var tableName = req.body.TableName
  var tableData = req.body.TableData
  var insertmaster = []
  for (var i = 0; i < tableData.length; i++) {
    var arrayValues = Object.values(tableData[i])
    insertmaster.push(arrayValues)
  }
  var tableFirstData = tableData[0]
  var arrayKeys = Object.keys(tableFirstData)
  // Decryption Code
  req.body.dbInfo.user = getDecryptKey(req.body.dbInfo.user)
  req.body.dbInfo.password = getDecryptKey(req.body.dbInfo.password)

  try {
    var poolCon = getCon(req.body.dbInfo)
  } catch (err) {
    return res.status(400).send({ message: 'Failure -: ' + err })
  }

  poolCon.getConnection(function (err, connection) {
    if (err) {
      return res.status(500).send({ message: "Can't connect to database." })
    } else {
      try {
        var url = "SHOW TABLES LIKE '" + tableName + "'"
        connection.query(url, (err, rows) => {
          if (err) {
            connection.destroy()
            return res.status(400).send({ message: 'Failure' + err })
          } else {
            var chlecklength = rows.length
            if (chlecklength > 0) {
              var url = 'desc ' + tableName
              connection.query(url, (err, rows) => {
                if (err) {
                  connection.destroy()
                  return res.status(400).send({ message: 'Failure' + err })
                } else {
                  arrayKeys.forEach((t, index) => {
                    let ind = rows.findIndex(r => r.Field == t)
                    if (ind !== -1) {
                      if (rows[ind].Type == 'datetime' || rows[ind].Type == 'date') {
                        insertmaster.forEach((m) => {
                          m[index] = new Date(m[index])
                        })
                      }
                    }
                  })

                  var inserturl = 'INSERT INTO ' + tableName + ' (' + arrayKeys + ') values ?'
                  connection.query(inserturl, [insertmaster], (err, rows) => {
                    if (err) {
                      connection.destroy()
                      return res.status(400).send({ message: 'Failure' + err })
                    } else {
                      connection.destroy()
                      res.status(200).send({ message: 'Success' })
                    }
                  })
                }
              })
            } else {
              connection.destroy()
              res.status(200).send({ message: 'Table not existing' })
            }
          }
        })
      } catch (err) {
        return res.status(400).send({ message: err })
      }
    }
  })
}

module.exports.exportData = exportData

let errorMsg = 'Failed To Fetch Database List.'
let mysql = require('mysql')

let connectDB = function (dbDetails, connType) {
  let deferred = q.defer()
  mongoPool(dbDetails, connType)
    .then(dbRes => {
      deferred.resolve(dbRes)
    })
    .catch(err => {
      deferred.reject(err)
    })
  return deferred.promise
}

let formmatRes = function (details) {
  let deferred = q.defer()
  let resultArray = []
  try {
    _.each(details.data, d => {
      if (details.keyName.toLowerCase() === 'database') {
        resultArray.push({
          Database: d.name
        })
      } else {
        resultArray.push({
          TABLE_NAME: d.name
        })
      }
    })
    deferred.resolve(resultArray)
  } catch (err) {
    deferred.reject({ message: errorMsg })
  }
  return deferred.promise
}

function getDecryptKey (value) {
  let secreKey = cryptoJs.enc.Utf8.parse(secretkey)
  let iv = cryptoJs.enc.Utf8.parse(secretkey)
  let decrypted = cryptoJs.AES.decrypt(value, secreKey, {
    keySize: 128 / 8,
    iv: iv,
    mode: cryptoJs.mode.CBC,
    padding: cryptoJs.pad.Pkcs7
  })
  return decrypted.toString(cryptoJs.enc.Utf8)
}

function getEncryptKey (value) {
  let secreKey = cryptoJs.enc.Utf8.parse(secretkey)
  var iv = cryptoJs.enc.Utf8.parse(secretkey)
  var encrypted = cryptoJs.AES.encrypt(cryptoJs.enc.Utf8.parse(value.toString()), secreKey,
    {
      keySize: 128 / 8,
      iv: iv,
      mode: cryptoJs.mode.CBC,
      padding: cryptoJs.pad.Pkcs7
    })

  return encrypted.toString()
}

function mysqlConnect (details) {
  let connection = mysql.createPool({
    connectionLimit: 200,
    host: details.host,
    user: details.user,
    password: details.password,
    database: details.sql_db,
    timezone: 'UTC+0',
    multipleStatements: true
  })

  return connection
}

module.exports.connectDB = connectDB
module.exports.formmatRes = formmatRes
module.exports.getDecryptKey = getDecryptKey
module.exports.getEncryptKey = getEncryptKey
module.exports.mysqlConnect = mysqlConnect

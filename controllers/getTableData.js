let mysqlConnect = require('./commonFunctions').mysqlConnect
let connectDB = require('./commonFunctions').connectDB
const getConnDetails = require('./commonFunctions').getConnDetails
let pool = require('../utils/db')

async function mysqlGetTableData (reqBody) {
  let deferred = q.defer()
  let poolCon = ''
  let dbName = ''
  try {
    poolCon = await mysqlConnect(reqBody.dbDetails)
    dbName = reqBody.db
  } catch (err) {
    return deferred.reject(err)
  }
  try {
    let tablename = reqBody.tableName
    let rowLimit = reqBody.rowLimit
    let orderByCol = reqBody.orderByColumn
    let orderType = reqBody.orderType
    let fil = reqBody.filter
    let andQ = ''
    let whereColStr = ''
    Object.keys(fil).forEach(k => (!fil[k] && fil[k] !== undefined) && delete fil[k])
    let keys = Object.keys(fil)
    let vals = Object.values(fil)
    if (keys.length > 0) {
      for (let i in keys) {
        // Check for "And" conditions
        if (keys.length - 1 == i) {
          andQ = ''
        } else {
          andQ = ' and '
        }
        // Building "WHERE" condition
        if (!isNaN(vals[i])) {
          whereColStr += keys[i] + ' = ' + vals[i] + andQ
        } else {
          let values = vals[i].type
          if (values == 'number') {
            if (_.isEmpty(vals[i].to) && vals[i].to === '') {
              whereColStr += keys[i] + ' = ' + vals[i].from + andQ
            } else {
              whereColStr += keys[i] + ' >= ' + vals[i].from + ' and ' + keys[i] + ' <= ' + vals[i].to + andQ
            }
          } else if (values == 'text') {
            if (_.isEmpty(vals[i].to) && vals[i].to === '') {
              whereColStr += keys[i] + " like '%" + vals[i].from + "%'" + andQ
            } else {
              whereColStr += keys[i] + ' >= ' + "'" + vals[i].from + "'" + ' and ' + keys[i] + ' <= ' + "'" + vals[i].to + "'" + andQ
            }
          } else if (values == 'datetime') {
            if (_.isEmpty(vals[i].to) && vals[i].to === '') {
              whereColStr += 'date(' + keys[i] + ") ='" + vals[i].from + "'" + andQ
            } else {
              whereColStr += 'date(' + keys[i] + ') >= ' + "'" + vals[i].from + "'" + ' and ' + 'date(' + keys[i] + ') <= ' + "'" + vals[i].to + "'" + andQ
            }
          } else {
            whereColStr += keys[i] + " ='" + vals[i].from + "'" + andQ
          }
        }
      }
      whereColStr = ' where ' + whereColStr
    }
    let orderQ = ''
    if (orderByCol == '') {
      orderQ = ''
    } else {
      orderQ = ' order by ' + orderByCol + ' ' + orderType
    }

    let url = 'select * from ' + tablename
    let getQuery = url + whereColStr + orderQ + ' limit ' + rowLimit

    poolCon.getConnection(function (err, connection) {
      if (err) {
        return deferred.reject({ message: "Can't connect to database." })
      } else {
        connection.query(getQuery, (err, rows) => {
          if (err) {
            connection.destroy()
            return deferred.reject({ Status: 'Failure1:-' + err })
          } else {
            if (!_.isEmpty(rows)) {
              if ((Object.keys(rows[0])).toString().includes('IsActive')) {
                _.each(rows, rs => {
                  let val1 = rs.IsActive
                  let val2 = rs.IsDelete
                  if (!_.isEmpty(val1)) {
                    for (const b of val1) {
                      rs.IsActive = b
                    }
                  }
                  if (!_.isEmpty(val2)) {
                    for (const a of val2) {
                      rs.IsDelete = a
                    }
                  }
                })
                connection.destroy()
                deferred.resolve(rows)
              } else {
                connection.destroy()
                try {
                  rows.forEach((t, index) => {
                    Object.keys(t).forEach(key => {
                      let val = t[key]
                      if (Buffer.isBuffer(val)) {
                        let bufferVal = val.toString('hex')
                        if (bufferVal === '01') {
                          t[key] = 1
                        } else { rows[index][key] = 0 }
                      }
                    })
                  })
                  deferred.resolve(rows)
                } catch (error) {
                  connection.destroy()
                  return deferred.reject({ message: 'Failure' + error })
                }
              }
            } else {
              connection.destroy()
              deferred.resolve({ message: 'No Record Found' })
            }
          }
        })
      }
    })
  } catch (err) {
    return deferred.reject({ Status: 'Failure:-' + err })
  }

  return deferred.promise
}

function mongodbGetTableData (reqBody, dbInfo, dataSource) {
  let qryArray = []
  let deferred = q.defer()
  let qryObject = reqBody.filter
  if (!_.isEmpty(qryObject)) {
    for (const qryObjectKey in qryObject) {
      if (_.isNaN(parseInt(qryObject[qryObjectKey]['from'])) && _.isNaN(parseInt(qryObject[qryObjectKey]['to']))) {
        qryArray.push({
          [qryObjectKey]: qryObject[qryObjectKey]['from']
        })
      } else {
        let parseValue = parseInt(qryObject[qryObjectKey]['from'])
        if (parseValue === qryObject[qryObjectKey]['from']) {
          qryArray.push({
            [qryObjectKey]: {
              $gte: parseInt(qryObject[qryObjectKey]['from']),
              $lte: parseInt(qryObject[qryObjectKey]['to'])
            }
          })
        } else {
          if (_.isEmpty(qryObject[qryObjectKey]['to'])) {
            qryArray.push({
              [qryObjectKey]: qryObject[qryObjectKey]['from']
            })
          } else {
            qryArray.push({
              [qryObjectKey]: {
                $gte: qryObject[qryObjectKey]['from'],
                $lte: qryObject[qryObjectKey]['to']
              }
            })
          }
        }
      }
      // }
    }
  }
  let dbDetails = {
    conDetails: dbInfo,
    commandType: 'findWithQuery',
    collecName: reqBody.tableName,
    query: !_.isEmpty(qryArray) ? qryArray[0] : {},
    limit: reqBody.rowLimit
  }
  connectDB(dbDetails, dataSource)
    .then(dbRes => {
      if (_.isEmpty(dbRes)) {
        deferred.resolve({
          message: 'No Data Found'
        })
      } else {
        deferred.resolve(dbRes)
      }
    })
    .catch(err => {
      deferred.reject(err)
    })

  return deferred.promise
}

function getTableData (req, res) {
  let reqBody = req.body
  let dataSource = reqBody.dataSource

  if (!_.isEmpty(dataSource) && dataSource.toLowerCase() === 'mysql') {
    mysqlGetTableData(reqBody)
      .then((result) => {
        res.status(200).send(result)
      })
      .catch((err) => {
        res.status(400).send(err)
      })
  } else if (!_.isEmpty(dataSource) && dataSource.toLowerCase() === 'mongodb') {
    // let encodePassword = encodeURIComponent(password)
    // reqBody.dbInfo.password = encodePassword
    mongodbGetTableData(reqBody, reqBody.dbDetails, 'getData')
      .then((result) => {
        res.status(200).send(result)
      })
      .catch((err) => {
        res.status(400).send(err)
      })
  } else {
    res.status(200).send({
      message: !_.isEmpty(dataSource) ? 'This DataSource is not exists' : 'DataSource Is Required'
    })
  }
}

function getAllDataFromDb (req, res) {
  let tableQuery = req.body.queryWithTableName
  let statement = getDecryptKey(tableQuery)
  try {
    pool.getConnection(function (err, connection) {
      if (err) {
        return res.status(500).send({ message: "Can't connect to database." })
      } else {
        connection.query(statement, (err, rows) => {
          if (err) {
            connection.destroy()
            return res.status(400).send({ message: err })
          } else {
            connection.destroy()
            return res.status(200).send({ data: rows })
          }
        })
      }
    })
  } catch (error) {
    return res.status(400).send({ message: error })
  }
}

module.exports.getTableData = getTableData
module.exports.getAllDataFromDb = getAllDataFromDb

const getConnDetails = require('./commonFunctions').getConnDetails

function mysqlListOfDatabase (dbInfoID) {
  let deferred = q.defer()
  try {
    pool.getConnection(function (err, connection) {
      if (err) {
        return deferred.reject({ message: "Can't connect to database." })
      } else {
        connection.query(`select conProfile from YDATAVIEWPROFILE where id = ${dbInfoID}`, (err, rows) => {
          connection.destroy()
          if (err) {
            return deferred.reject({ Status: 'Failure -: ' + err })
          } else {
            // console.log('rows', rows)
            deferred.resolve(rows)
          }
        })
      }
    })
  } catch (err) {
    return deferred.reject({ Status: 'Failure -: ' + err })
  }

  return deferred.promise
}

function getListOfDatabase (req, res) {
  let reqBody = req.body
  let dbInfoID = getDecryptKey(reqBody.dbInfo)
  // let domainId = getDecryptKey(req.body.domainId)
  let dataSource = reqBody.dataSource
  if (!_.isEmpty(dataSource) && dataSource.toLowerCase() === 'mysql') {
    mysqlListOfDatabase(dbInfoID)
      .then((finalRes) => {
        let dbArray = []
        if (_.isEmpty(finalRes) || finalRes.length === 0) {
          res.status(200).send({
            message: 'No Database Found.'
          })
        } else {
          _.each(finalRes, r => {
            dbArray.push({
              Database: r.conProfile
            })
          })
          res.status(200).send(dbArray)
        }
      })
      .catch((err) => {
        res.status(400).send(err)
      })
  } else if (!_.isEmpty(dataSource) && dataSource.toLowerCase() === 'mongodb') {
    getConnDetails(reqBody)
      .then((result1) => {
        res.status(200).send([
          {
            'Database': result1.sql_db
          }
        ])
      })
  } else {
    res.status(200).send({
      message: !_.isEmpty(dataSource) ? 'This DataSource is not exists' : 'DataSource Is Required'
    })
  }
}

module.exports.getListOfDatabase = getListOfDatabase

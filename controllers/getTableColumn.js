let mysqlConnect = require('./commonFunctions').mysqlConnect
let connectDB = require('./commonFunctions').connectDB
let errorMsg = 'Failed To Fetch Records.'

async function mysqlGetTableColumn (reqBody) {
  let deferred = q.defer()
  var poolCon = ''
  try {
    poolCon = await mysqlConnect(reqBody.dbDetails)
  } catch (err) {
    return deferred.reject(err)
  }
  try {
    var tablename = reqBody.TABLE_NAME
    poolCon.getConnection(function (err, connection) {
      if (err) {
        return deferred.reject({ message: "Can't connect to database." })
      } else {
        let url = 'SHOW COLUMNS FROM ' + tablename
        connection.query(url, (err, rows) => {
          if (err) {
            connection.destroy()
            return deferred.reject({ Status: 'Failure' + err })
          } else {
            for (let i in rows) {
              if (rows[i].Type.includes('int')) {
                if (rows[i].Type.includes('tinyint')) {
                  rows[i].maxlenth = 1
                  rows[i].Type = 'tinyint'
                } else {
                  rows[i].maxlenth = 10
                  rows[i].Type = 'int'
                }
              } else if (rows[i].Type.includes('varchar')) {
                rows[i].maxlenth = parseInt((rows[i].Type.replace('varchar(', '').replace(')', '')))
                rows[i].Type = 'varchar'
              } else {
                rows[i].maxlenth = 100
              }
            }
            connection.destroy()
            deferred.resolve(rows)
          }
        })
      }
    })
  } catch (err) {
    return deferred.reject({ Status: 'Failure :-' + err })
  }

  return deferred.promise
}

let getDataTypes = function (object) {
  let deferred = q.defer()
  let fieldAr = []
  let fieldObj = {}
  delete object['_id']
  try {
    for (const key in object) {
      if (((new Date(object[key])) !== 'Invalid Date') || typeof (object[key]) === 'object') {
        let dataType = (new Date(object[key]).toLocaleDateString()) === 'Invalid Date' ? typeof (object[key]) : 'date'
        if (!_.has(fieldObj, key)) {
          fieldObj['Field'] = key
          fieldObj['Type'] = dataType
          fieldObj['Maxlength'] = 10
        }
        if (Array.isArray(object[key])) {
          fieldObj['Field'] = key
          fieldObj['Type'] = 'array'
          fieldObj['Maxlength'] = 20
        }
      } else if (typeof (object[key]) === 'number') {
        if (Number.isInteger(object[key])) {
          fieldObj['Field'] = key
          fieldObj['Type'] = 'number'
          fieldObj['Maxlength'] = 20
        } else {
          fieldObj['Field'] = key
          fieldObj['Type'] = 'double'
          fieldObj['Maxlength'] = 20
        }
      } else {
        fieldObj['Field'] = key
        fieldObj['Type'] = typeof (object[key])
        fieldObj['Maxlength'] = 20
      }
      fieldAr.push({
        Field: fieldObj.Field,
        Type: fieldObj.Type,
        Maxlength: fieldObj.Maxlength
      })
    }
    deferred.resolve(fieldAr)
  } catch (error) {
    // console.log('error', error)
    deferred.reject({ message: errorMsg })
  }

  return deferred.promise
}

function getTableColumns (dbInfo, dataSource, TABLE_NAME) {
  let deferred = q.defer()
  let dbDetails = {
    conDetails: dbInfo,
    commandType: 'getTableColumns',
    collecName: TABLE_NAME,
    query: {}
  }
  connectDB(dbDetails, dataSource)
    .then(dbRes => {
      if (_.isEmpty(dbRes)) {
        deferred.resolve({
          message: 'No Data Found'
        })
      } else {
        return getDataTypes(dbRes)
      }
    })
    .then(finalRes => {
      deferred.resolve(finalRes)
    })
    .catch(err => {
      deferred.reject(err)
    })

  return deferred.promise
}

function getTableColumn (req, res) {
  let reqBody = req.body
  let dataSource = reqBody.dataSource
  let TABLE_NAME = reqBody.TABLE_NAME
  if (!_.isEmpty(dataSource) && dataSource.toLowerCase() === 'mysql') {
    mysqlGetTableColumn(reqBody)
      .then((result) => {
        res.status(200).send(result)
      })
      .catch((err) => {
        res.status(400).send(err)
      })
  } else if (!_.isEmpty(dataSource) && dataSource.toLowerCase() === 'mongodb') {
    // let encodePassword = encodeURIComponent(password)
    // reqBody.dbInfo.password = encodePassword
    getTableColumns(reqBody, 'getData', TABLE_NAME)
      .then((result) => {
        res.status(200).send(result)
      })
      .catch((err) => {
        res.status(400).send(err)
      })
  } else {
    res.status(200).send({
      message: !_.isEmpty(dataSource) ? 'This DataSource is not exists' : 'DataSource Is Required'
    })
  }
}

module.exports.getTableColumn = getTableColumn

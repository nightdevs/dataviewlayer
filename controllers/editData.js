let getCon = require('./commonFunctions').getCon

function getPrimaryKey (poolCon, database, tableName) {
  console.log('table', tableName)
  console.log('db', database)
  let deferred = q.defer()
  poolCon.getConnection(function (err, connection) {
    if (err) {
      return deferred.reject({ message: "Can't connect to database." })
    } else {
      let query = 'describe ' + database + '.' + tableName + ';'
      console.log('query', query)
      connection.query(query, (err, rows) => {
        if (err) {
          return deferred.reject({ message: err })
        } else {
          let keydata = []
          let checkpri = 'PRI'
          _.each(rows, rs => {
            if (rs.Key === checkpri) {
              keydata.push(rs.Field)
            }
          })
          return deferred.resolve({ 'keydata': keydata, 'rows': rows })
        }
      })
    }
  })
  return deferred.promise
}
function comparePriColumnwithInputPayload (primaryCol, dataBody, db, tablename) {
  let deferred = q.defer()
  let arrayKeys = Object.keys(dataBody)
  let primaryColumn = primaryCol.keydata
  let AllColumn = primaryCol.rows
  console.log('AllColumn', AllColumn)
  console.log('db', db)
  let obj = {}
  let andQ = ''
  let whereCol = ''
  for (let c in dataBody) {
    _.each(primaryColumn, pc => {
      if (c === pc) {
        obj[c] = dataBody[c]
      }
      // finalData.push({ [c]: dataBody[c] })
    })
  }
  console.log('arraykeys====>', arrayKeys)
  arrayKeys.forEach((t, index) => {
    console.log('t===>', t)
    console.log('index===>', index)
    let ind = AllColumn.findIndex(r => r.Field == t)
    if (ind !== -1) {
      if (AllColumn[ind].Type == 'date') {
        dataBody[t] = new Date(dataBody[t]).toJSON().split('T')[0]
      } else if (AllColumn[ind].Type == 'datetime') {
        dataBody[t] = new Date(dataBody[t]).toJSON().split('T')[0] + ' ' +
         new Date(dataBody[t]).toJSON().split('T')[1].split('.')[0]
      }
    }
  })
  // where condition
  Object.keys(obj).forEach(k => (!obj[k] && obj[k] !== undefined) && delete obj[k])
  var wherekeys = Object.keys(obj)
  var wherevals = Object.values(obj)
  console.log('where', wherekeys)
  if (!_.isEmpty(wherekeys)) {
    if (wherekeys.length > 0) {
      for (let i in wherekeys) {
        // Check for "And" conditions
        if (wherekeys.length - 1 == i) {
          andQ = ''
        } else {
          andQ = ' and '
        }

        whereCol += wherekeys[i] + ' = ' + "'" + wherevals[i] + "'" + andQ
      }
    }
  } else {
    deferred.reject('No primary key')
  }

  // remove primary object from payload
  _.each(wherekeys, wk => {
    delete dataBody[wk] // Removes json.foo from the dictionary.
  })
  // edit condition
  let comma = ''
  let editqry = ''

  Object.keys(dataBody).forEach(k => (!dataBody[k] && dataBody[k] !== undefined) && delete dataBody[k])
  var editkeys = Object.keys(dataBody)
  var editvals = Object.values(dataBody)
  if (editkeys.length > 0) {
    for (let j in editkeys) {
      if (editkeys.length - 1 == j) {
        comma = ''
      } else {
        comma = ', '
      }

      editqry += editkeys[j] + ' = ' + "'" + editvals[j] + "'" + comma
    }
  }
  let whereqry = ' where ' + whereCol
  let query = 'update ' + db + '.' + tablename + ' set ' + editqry + whereqry + ';'
  console.log('query', query)
  deferred.resolve(query)
  return deferred.promise
}

function updateData (poolCon, createdQuery) {
  console.log('createdQuery', createdQuery)
  let deferred = q.defer()
  poolCon.getConnection(function (err, connection) {
    if (err) {
      return deferred.reject({ message: "Can't connect to database." })
    } else {
      connection.query(createdQuery, (err, result) => {
        if (err) {
          return deferred.reject({ message: err })
        } else {
          console.log('result', result)
          if (result.affectedRows >= 0) {
            return deferred.resolve({ message: 'Data updated successfully' })
          } else {
            return deferred.reject({ message: 'Fail to updated Data' })
          }
        }
      })
    }
  })
  return deferred.promise
}

async function editData (req, res) {
  let reqBody = req.body
  let header = reqBody.header
  let db = header.db
  let tablename = header.tableName
  let connectID = getDecryptKey(header.dbInfo)
  let dataBody = reqBody.dataBody

  var poolCon = ''
  try {
    poolCon = await getCon(connectID)
      .catch((err) => {
        console.log(err)
      })
  } catch (err) {
    return err
  }
  getPrimaryKey(poolCon, db, tablename)
    .then((primaryColumn) => {
      return comparePriColumnwithInputPayload(primaryColumn, dataBody, db, tablename)
    })
    .then((createdQuery) => {
      return updateData(poolCon, createdQuery)
    })
    .then((Results) => {
      return res.status(200).send(Results)
    })
    .catch((err) => {
      return res.status(400).send({ Status: err })
    })
}
module.exports.editData = editData

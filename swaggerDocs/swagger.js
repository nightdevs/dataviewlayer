
/* getTable Api */
/**
 * @route POST /getTable
 * @group getTable - Operations about get Table Name
* @param {dbDataa.model} data.body.required - the data
 * @returns {object} 200 - It gives the success message
 * @returns {Error}  default - Unexpected error
 */
/**
 * @typedef dbDataa
 * @property {string} dbInfo.required
 * @property {string} db.required
 * @property {string} dataSource.required
 */
/**
 * @typedef Error
 * @property {string} error
*/
/**
 * @typedef Response
 * @property {string} response
*/
/* getListOfDatabase */
/**
 * @route POST /getListOfDatabase
 * @group getListOfDatabase - It is used for get DataBase List
 * @param {listDbData.model} data.body.required - the data
 * @returns {object} 200 - It gives the success message
 * @returns {Error}  default - Unexpected error
 */
/**
 * @typedef listDbData
 * @property {string} dbInfo.required
 * @property {string} dataSource.required
 */
/**
 * @typedef Error
 * @property {string} error
 */
/**
 * @typedef Response
 * @property {string} response
*/
/* getTableColumn */
/**
 * @route POST /getTableColumn
 * @group getTableColumn - It is used for get All Table Column
 * @param {columnData.model} data.body.required - the data
 * @returns {object} 200 - It gives the success message
 * @returns {Error}  default - Unexpected error
 */
/**
 * @typedef columnData
 * @property {string} dbInfo.required
 * @property {string} dataSource.required
 * @property {string} db.required
 * @property {string} TABLE_NAME.required
*/
/**
 * @typedef Error
 * @property {string} error
 */
/**
 * @typedef Response
 * @property {string} response
*/
/* getTableData */
/**
 * @route POST /getTableData
 * @group getTableData - It is used for get Table Data
 * @param {tableData.model} data.body.required - the data
 * @returns {object} 200 - It gives the success message
 * @returns {Error}  default - Unexpected error
 */
/**
 * @typedef tableData
 * @property {string} dbInfo.required
 * @property {string} db.required
 * @property {string} dataSource.required
 * @property {string} domainId.required
 * @property {string} tableName.required
 * @property {integer} rowLimit.required
 * @property {string} orderByColumn.required
 * @property {string} orderType.required
 * @property {object} filter
*/
/**
 * @typedef Error
 * @property {string} error
 */
/**
 * @typedef Response
 * @property {arrayofobject} response
*/
/* saveProfile */
/**
 * @route POST /saveProfile
 * @group saveProfile - It is used for save connection
 * @param {saveprof.model} data.body.required - the data
 * @returns {object} 200 - It gives the success message
 * @returns {Error}  default - Unexpected error
 */
/**
 * @typedef saveprof
 * @property {string} domainID.required
 * @property {string} connProfile.required
 * @property {string} connType.required
 * @property {dbdatamodel.model} connInfo
 */
/**
 * @typedef Error
 * @property {string} Status.required
 */
/**
 * @typedef DataBaseResponse
 * @property {string} Status.required
 * @property {arrayofobject} data.required
*/
/* getConnProfiles */
/**
 * @route POST /getConnProfiles
 * @group getConnProfiles - It is used for get Connection List
 * @param {conndata.model} data.body.required - the data
 * @returns {object} 200 - It gives the success message
 * @returns {Error}  default - Unexpected error
 */
/**
 * @typedef conndata
 * @property {string} domainID.required
 */
/**
 * @typedef Error
 * @property {string} error
 */
/**
 * @typedef Response
 * @property {string} response
*/
/* saveColumns Api */
/**
 * @route POST /saveColumns
 * @group saveColumns - Operations about save columns
 * @param {coldata.model} data.body.required - the data
 * @returns {object} 200 - It gives the success message
 * @returns {Error}  default - Unexpected error
 */
/**
 * @typedef coldata
 * @property {string} sql_db.required
 * @property {integer} conId.required
 * @property {string} Table_Name.required
 * @property {string} domainID.required
 * @property {string} LAYOUT_NAME.required
 * @property {string} desc.required
 * @property {Array.<object>} COLS.required
 */
/**
 * @typedef Error
 * @property {string} Status.required
 */
/**
 * @typedef saveColResponse
 * @property {string} Status.required
*/
/* getLayoutView Api */
/**
 * @route POST /getLayoutView
 * @group getLayoutView - Operation about get layout
 * @param {layoutdata.model} data.body.required - the data
 * @returns {object} 200 - It fives the success message
 * @returns {Error} default - Unexpected error
 */
/**
 * @typedef layoutdata
 * @property {string} sql_db.required
 * @property {integer} conId.required
 * @property {string} Table_Name.required
 * @property {string} domainID.required
 */
/**
 * @typedef Error
 * @property {string} Status.required
 */
/**
 * @typedef getsaveColResponse
 * @property {string} message.required
 * @property {arrayofobject} Data
*/
/* getSaveColLayoutView Api */
/**
 * @route POST /getSaveColLayoutView
 * @group getSaveColLayoutView - Operation about get layout
 * @param {layoutdata.model} data.body.required - the data
 * @returns {object} 200 - It fives the success message
 * @returns {Error} default - Unexpected error
 */
/**
 * @typedef layoutdata
 * @property {string} sql_db.required
 * @property {integer} conId.required
 * @property {string} Table_Name.required
 * @property {string} domainID.required
 */
/**
 * @typedef Error
 * @property {string} Status.required
 */
/**
 * @typedef getsaveColResponse
 * @property {string} message.required
 * @property {arrayofobject} Data
*/
/* getSaveColumns Api */
/**
 * @route get /getSaveColumns
 * @group getSaveColumns - Operation about get save Columns
 * @param {integer} id.query.required - id - eg: null
 * @returns {object} 200 - An object of role information
 * @returns {Error}  default - Unexpected error
 */
/**
 * @typedef Error
 * @property {string} message.required
 */
/**
 * @typedef objectResponse
 * @property {arrayofobject} data
 */
/* getSaveColumnsValues */
/**
 * @route get /getSaveColumnsValues
 * @group getSaveColumnsValues - Operation about get save Columns
 * @param {integer} id.query.required - id - eg: null
 * @returns {object} 200 - An object of role information
 * @returns {Error}  default - Unexpected error
 */
/**
 * @typedef Error
 * @property {string} message.required
 */
/**
 * @typedef objectResponse
 * @property {arrayofobject} data
 */
/* saveColumnsValue */
/**
 * @route POST /saveColumnsValue
 * @group saveColumnsValue - Operation about get layout
 * @param {savecoldata.model} data.body.required - the data
 * @returns {object} 200 - It fives the success message
 * @returns {Error} default - Unexpected error
 */
/**
 * @typedef savecoldata
 * @property {string} sql_db.required
 * @property {integer} conId.required
 * @property {string} Table_Name.required
 * @property {string} domainID.required
 * @property {string} LAYOUT_NAME.required
 * @property {string} desc.required
 * @property {object} data.required
 */
/**
 * @typedef Error
 * @property {string} Status.required
 */
/**
 * @typedef getsaveColResponse
 * @property {string} Status.required
*/
/* editProfile */
/**
 * @route POST /editProfile
 * @group editProfile - Operation about edit profile
 * @param {editprof.model} data.body.required - the data
 * @returns {object} 200 - It fives the success message
 * @returns {Error} default - Unexpected error
 */
/**
 * @typedef editprof
 * @property {integer} id.required
 * @property {string} connProfile.required
 * @property {string} connType.required
 * @property {dbdatamodel.model} connInfo
 */
/**
 * @typedef Error
 * @property {string} error
 */
/**
 * @typedef Response
 * @property {string} response
 */
/* deleteProfile */
/**
 * @route POST /deleteProfile
 * @group deleteProfile - Operation about delete profile
 * @param {delprof.model} data.body.required - the data
 * @returns {object} 200 - It fives the success message
 * @returns {Error} default - Unexpected error
 */
/**
 * @typedef delprof
 * @property {integer} id.required
 */
/**
 * @typedef Error
 * @property {string} error
 */
/**
 * @typedef Response
 * @property {string} response
 */
/* getSchemaDetail */
/**
 * @route POST /getSchemaDetail
 * @group getSchemaDetail - Operation about get schema's table and procedure
 * @param {schemadtl.model} data.body.required - dbInfo is connection id in encrypted mode
 * @returns {object} 200 - It fives the success message
 * @returns {Error} default - Unexpected error
 */
/**
 * @typedef schemadtl
 * @property {string} dbInfo.required
 * @property {string} dbName.required
 */
/**
 * @typedef Error
 * @property {string} error
 */
/**
 * @typedef Response
 * @property {string} response
 */

let mysql = require('mysql')
let config = require('../config/config')
let util = require('util')

let connection = mysql.createPool({
  connectionLimit: 200,
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.sql_db,
  timezone: 'UTC+0',
  multipleStatements: true
})

connection.getConnection((err, connect) => {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.error('Database connection was closed.')
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
      console.error('Database has too many connections.')
    }
    if (err.code === 'ECONNREFUSED') {
      console.error('Database connection was refused.')
    }
  }
  if (connect) connect.release()
})
connection.query = util.promisify(connection.query)

module.exports = connection

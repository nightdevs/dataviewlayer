
let mongoose = require('mongoose')
let config = require('../config/config')

let mongoPool = function (details, connType) {
  // console.log('details', details)
  // console.log('connType', connType)
  let deferred = q.defer()
  if (config.isMongoAuthReq === true) {
    if (!_.isEmpty(connType) && connType.toLowerCase() === 'mongodb') {
      mongoose.connect(`mongodb://${details.user}:${details.password}@${details.host}:27017/${details.sql_db}`).catch((err) => {
        return deferred.reject(err)
      })
      deferred.resolve({ message: 'mongodb connected successfully' })
    } else {
      mongoose.connect(`mongodb://${details.conDetails.user}:${details.conDetails.password}@${details.conDetails.host}:27017/${details.conDetails.sql_db}`).catch((err) => {
        return deferred.reject(err)
      })
      mongoose.connection.on('open', function () {
        if (details.commandType.toLowerCase() === 'getcollections') {
          mongoose.connection.db.listCollections().toArray(function (err, names) {
            if (err) {
              return deferred.reject(err)
            } else {
            // console.log('names', names)
              deferred.resolve(names)
            }
          })
        } else if (details.commandType.toLowerCase() === 'findwithquery') {
          mongoose.connection.db.collection(String(details.collecName)).find(details.query).limit(Number(details.limit)).toArray((err, result) => {
            if (err) {
              return deferred.reject(err)
            } else {
              deferred.resolve(result)
            }
          })
        } else {
          mongoose.connection.db.collection((details.collecName)).findOne((err, result) => {
          // console.log('result', result)
          // console.log('err', err)
            if (err) {
              return deferred.reject(err)
            } else {
              deferred.resolve(result)
            }
          })
        }
      // mongoose.connection.close()
      })
    }
  } else {
    if (!_.isEmpty(connType) && connType.toLowerCase() === 'mongodb') {
      let url = `mongodb://${details.host}:27017/`
      // console.log('saveProfile call', url)
      MongoClient.connect(url, { useNewUrlParser: true }, (err, db) => {
        if (err) {
          deferred.reject(err)
        }
      })
      deferred.resolve({ message: 'mongodb connected successfully' })
    } else {
      let url = `mongodb://${details.conDetails.host}:27017/`
      // console.log('url', url)
      MongoClient.connect(url, { useNewUrlParser: true }, (err, db) => {
        if (err) {
          deferred.reject(err)
        } else {
          let dbo = db.db(String(details.conDetails.sql_db))
          if (details.commandType.toLowerCase() === 'getcollections') {
            dbo.listCollections().toArray((err, collInfos) => {
              if (err) {
                deferred.reject(err)
              } else {
                // console.log('collInfos', collInfos)
                deferred.resolve(collInfos)
              }
            })
          } else if (details.commandType.toLowerCase() === 'findwithquery') {
            // console.log('findwithquery')
            dbo.collection(String(details.collecName)).find(details.query).limit(Number(details.limit)).toArray((err, result) => {              
              if (err) {
                return deferred.reject(err)
              } else {
                deferred.resolve(result)
              }
            })
          } else {
            dbo.collection(String(details.collecName)).findOne((err, result) => {
              if (err) {
                deferred.reject(err)
              } else {
                deferred.resolve(result)
              }
            })
          }
          // db.close()
        }
      })
    }
  }
  // mongoose.connection.on('error', function (error) {
  //   console.log('connection error', error)
  //   return deferred.reject(error)
  // })
  return deferred.promise
}

module.exports.mongoPool = mongoPool
